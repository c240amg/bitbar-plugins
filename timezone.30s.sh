#!/bin/bash

# <bitbar.title>Timezones+</bitbar.title>
# <bitbar.version>v1.0</bitbar.version>
# <bitbar.author>Aaron Edell</bitbar.author>
# <bitbar.author.github>aaronedell</bitbar.author.github>
# <bitbar.desc>Rotates current time through four common timezones </bitbar.desc>
# <bitbar.image>http://i.imgur.com/Y4nhdZo.png</bitbar.image>
# <bitbar.dependencies>Bash GNU AWK</bitbar.dependencies>
echo  "⏰"
echo  "---"
echo -n "SFO " ; TZ=":US/Pacific" date +'🇺🇸 %H:%M %Y%m%d'
echo -n "NYC " ; TZ=":US/Eastern" date +'🇺🇸 %H:%M %Y%m%d'
echo -n "LDN " ; TZ=":Europe/London" date +'🇬🇧 %H:%M %Y%m%d'
echo -n "JPN " ; TZ=":Japan" date +'🇯🇵 %H:%M %Y%m%d'
echo -n "AUS " ; TZ=":Australia/Sydney" date +'🇦🇺 %H:%M %Y%m%d'