#!/bin/bash

# <bitbar.title>ip</bitbar.title>
# <bitbar.version>v1.0</bitbar.version>
# <bitbar.author>Phil Spencer</bitbar.author>
# <bitbar.author.github>c240amg</bitbar.author.github>
# <bitbar.desc>checks external IP address</bitbar.desc>

# This is a plugin of Bitbar
# https://github.com/matryer/bitbar

external_ip=`curl -s ipinfo.io/ip`
json=`curl -s http://ip-api.com/json`
ccc=`echo $json | /opt/local/bin/jq -c -M -r .countryCode`
cas=`echo $json | /opt/local/bin/jq -c -M -r .as`
crn=`echo $json | /opt/local/bin/jq -c -M -r .regionName`
# CC now contains the iso code GB
# split into single chars
fc="${ccc:0:1}"
sc="${ccc:1:1}"
# convert to decimal
fcdec=`printf '%d' "'$fc'"`
scdec=`printf '%d' "'$sc'"`
# echo "decimal $fcdec $scdec"
firstl=`echo "$((0x65 + $fcdec  ))"`
#secondl=`echo " $((0xA6 + 1 ))"`
secondl=`echo " $((0x65 + $scdec ))"`
# Convert to hex
firstl=`printf "%x" $firstl`
secondl=`printf "%x" $secondl`

echo -e "🌐"
echo "---"
echo "IP: $external_ip"
#echo "$firstl $secondl"
echo -e "\xF0\x9F\x87\x$firstl\xF0\x9F\x87\x$secondl"
#echo -e "\xF0\x9F\x87\xAC\xF0\x9F\x87\xA7"
echo "-- $cas"
echo "-- $crn"
echo "Refresh... | refresh=true"

# B = A7
# c = a8
# d = a9
# e = aa
# f = ab
# g = ac
# 
# a = 65
# 
# 
# 42  -a
# 43 bc
# 44d
# 45e
# 46f
# 47g
# 
# AC = 172
# A7 = 167